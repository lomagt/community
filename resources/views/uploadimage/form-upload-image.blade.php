@extends('layouts.app')
@section('content')
<div class="container">
    @include('flash-message')

    <h1>Sube tu Avatar</h1>
    <form action="/updateimage" method="post" enctype="multipart/form-data">
        @csrf
        <label class="custom-file">
            <input type="file" id="default0" class="browser-default @error('imagen') is-invalid @else is-valid @enderror" name="imagen">
            @error('imagen')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <span class="custom-file-control"></span>
        </label>
        <br />

        <input class="btn btn-outline-info mt-3" type="submit" value="Subir">
    </form>


</div>

@stop