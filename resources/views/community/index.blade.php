@extends('layouts.app')
@section('content')
<div class="container">
@include('flash-message')
    <h1 class="text-center bg-dark rounded-top text-white p-2">Community</h1>
    <div class="row">

        @include('community.partials.lists-links')
        
        @include('community.partials.add-link')

    </div>
    @if(count($links) === 0)
        <h4>"No contributions yet"</h4>
    @else
    {{$links->appends($_GET)->links()}}
    @endif

</div>

@stop