<div class="col-md-8">
    @if($channelActivo)
    <h1>
        <a href="/community">Community</a>
        <span>-{{$channel->title}}</span>
    </h1>
    @elseif(!empty($arrayMatchesUrl['buscador']))
    <h1>
        <a href="/community">Community</a>
        <span>-Resultados</span>
    </h1>
    @else
    <h1>Community</h1>
    @endif

    <ul class="nav">
        <li class="nav-item">
            @if(!empty($arrayMatchesUrl['popular']) && !empty($arrayMatchesUrl['buscador']))
            <!-- Comprueba que existe la palabra popular en la url si existe deja activo el link si no lo desativa
        en el href coje la url inicial en el request-->
            <a class="nav-link" href="{{request()->url()}}?buscador={{$buscador}}">Most recent</a>

            @else

            <a class="nav-link {{!empty($arrayMatchesUrl['popular']) ? '' : 'disabled' }}" href="{{request()->url()}}">Most recent</a>

            @endif

        </li>
        <li class="nav-item">
            @if(!empty($arrayMatchesUrl['popular']))
            <a class="nav-link disabled" href="{{request()->fullUrl()}}">
                Most popular
            </a>
            @elseif(!empty($arrayMatchesUrl['buscador']) && empty($arrayMatchesUrl['popular']))
            <a class="nav-link" href="{{request()->fullUrl()}}&popular">
                Most popular
            </a>
            @else
            <a class="nav-link" href="?popular">
                Most popular
            </a>
            @endif
        </li>
    </ul>


    @foreach ($links as $link)
    <li class="contribute d-flex p-2">
        <span class="label label-default mx-3 px-2 rounded rounded-3" style="background:{{ $link->channel->color }}">
            {{ $link->channel->title }}
        </span>
        <a class="mx-2" href="/community/{{ $link->channel->slug }}" target="_blank">
            {{$link->title}}
        </a>
        <small>Contributed by: {{$link->creator->name}} {{$link->updated_at->diffForHumans()}}</small>

        <div class="flex-grow-1 d-flex justify-content-end">
            <i class="mx-2 far fa-heart fs-3 text-secondary"></i>

            <form method="POST" action="/votes/{{ $link->id }}">
                {{ csrf_field() }}
                <button type="submit" class="btn {{ Auth::check() && Auth::user()->votedFor($link) ? 'btn-success' : 'btn-outline-secondary' }} p-0 px-2" {{ Auth::guest() ? 'disabled' : '' }}>
                    {{$link->users()->count()}}
                </button>
            </form>

        </div>
    </li>
    @endforeach
</div>