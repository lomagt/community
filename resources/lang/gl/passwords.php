<?php
return [
    'reset'     => 'Restableceuse o teu contrasinal.',
    'sent'      => 'Enviamos por correo electrónico a ligazón para restablecer o contrasinal.',
    'throttled' => 'Agarde antes de tentalo de novo.',
    'token'     => 'Restableceuse o teu contrasinal.',
    'user'      => 'Non podemos atopar un usuario con ese enderezo de correo electrónico.'
];