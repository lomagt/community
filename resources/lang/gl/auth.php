<?php
return [
    'failed'   => 'Estas credenciais non coinciden cos nosos rexistros.',
    'password' => 'O contrasinal proporcionado é incorrecto.'
];