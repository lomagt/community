<?php
return [
    'reset'     => 'Votre mot de passe a été réinitialisé!',
    'sent'      => 'Votre mot de passe a été réinitialisé!',
    'throttled' => 'Veuillez patienter avant de réessayer.',
    'token'     => 'Votre mot de passe a été réinitialisé!',
    'user'      => 'Nous ne pouvons pas trouver un utilisateur avec cette adresse e-mail.'
];