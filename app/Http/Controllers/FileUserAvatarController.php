<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\FileUserAvatarForm;
use App\Queries\UsersQuery;

class FileUserAvatarController extends Controller
{

    public function index()
    {
        return view('uploadimage/form-upload-image');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(FileUserAvatarForm $request)
    {
        
        $user = Auth::user();

        if((new UsersQuery)->setFileAvatarInUser($user, $request)){
            return back()->with('success', 'Imagen subida con exito!!!');
        }else{
            return back()->with('warn', 'El tamaño de la imagen o el formato no es válido!!!');
        }
    }
}
