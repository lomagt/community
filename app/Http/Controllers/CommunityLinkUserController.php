<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CommunityLink;
use App\Models\CommunityLinkUser;
use Illuminate\Support\Facades\Auth;

class CommunityLinkUserController extends Controller
{

    public function store(CommunityLink $link)
    {
        //La primera linea busca registros en la base de datos que coincidan con los parametros dados en la tabla CommunityLinkUser
        // y devuelve el modelo, si no encuetra nada crea una instacia del modelo que debe ser guardada para que se conserven los datos
        // $vote = CommunityLinkUser::firstOrNew(['user_id' => Auth::id(), 'community_link_id' => $link->id]);
        // if ($vote->id) { Aqui comprueba que tiene un id, si lo tiene es que en la busqueda anterior se encontró
        // un registro coincidente por lo que el modelo existia anteriormente    
        //    $vote->delete(); Aqui lo borra
        // } else {
        //   $vote->save(); Aqui lo guarda si no hay registros anteriores
        //}


        CommunityLinkUser::firstOrNew([
            'user_id' => Auth::id(),
            'community_link_id' => $link->id
        ])->toggle();

        return back();
    }
}
