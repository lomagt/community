<?php

namespace App\Http\Controllers;

use App\Models\CommunityLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Channel;
use App\Models\User;
use App\Http\Requests\CommunityLinkForm;
use App\Queries\CommunityLinksQuery;
use App\Queries\UsersQuery;

class CommunityLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Channel $channel = null)
    {
        $buscador = ' ';
        $channels = Channel::orderBy('title', 'asc')->get();
        $url = urldecode(parse_url(request()->fullUrl(), PHP_URL_QUERY));

        $matchUrlBuscador = '';
        $matchUrlPopular = '';
        preg_match('/buscador/', $url, $matchUrlBuscador);
        preg_match('/popular/', $url, $matchUrlPopular);

        $arrayMatchesUrl = [];

        if (!empty($matchUrlPopular) && $channel !== null && empty($matchUrlBuscador)) {

            $arrayMatchesUrl = ['popular' => $matchUrlPopular];

            $links = (new CommunityLinksQuery)->getMostPopularWithChannel($channel);
            $channelActivo = true;
        } else if (!empty($matchUrlPopular) && $channel === null && empty($matchUrlBuscador)) {

            $arrayMatchesUrl = ['popular' => $matchUrlPopular];

            $links = (new CommunityLinksQuery)->getMostPopular();
            $channelActivo = false;
        } else if ($channel !== null && empty($matchUrlPopular) && empty($matchUrlBuscador)) {

            $links = (new CommunityLinksQuery)->getByChannel($channel);

            $channelActivo = true;
        } else if (!empty($matchUrlBuscador) && empty($matchUrlPopular) && $channel === null) {

            $arrayMatchesUrl = ['buscador' => $matchUrlBuscador[0]];

            $buscador = request()->buscador;

            $links = (new CommunityLinksQuery)->getSearchLinks($buscador);

            $channelActivo = false;
        } else if (!empty($matchUrlBuscador) && !empty($matchUrlPopular) && $channel === null) {

            $arrayMatchesUrl = ['buscador' => $matchUrlBuscador[0], 'popular' => $matchUrlPopular[0]];

            $buscador = request()->buscador;

            $links = (new CommunityLinksQuery)->getSearchLinksWithPopular($buscador);

            $channelActivo = false;
        } else {

            $arrayMatchesUrl = ['buscador' => '', 'popular' => ''];

            $links = (new CommunityLinksQuery)->getAll();
            $channelActivo = false;
        }



        return view('community/index', compact('links', 'channels', 'channelActivo', 'channel', 'buscador', 'arrayMatchesUrl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommunityLinkForm $request)
    {

        $link = new CommunityLink();
        $link->user_id = Auth::id();

        $approved = Auth::User()->isTrusted() ? true : false;
        $request->merge(['user_id' => Auth::id(), 'approved' => $approved]);

        if ($link->hasAlreadyBeenSubmitted($request->link)) {
        } else {
            CommunityLink::create($request->all());
        }


        if ($approved) {
            return back()->with('success', 'Aproved User');
        } else {
            return back()->with('error', 'Unaproved User');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function edit(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommunityLink $communityLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityLink $communityLink)
    {
        //
    }
}
