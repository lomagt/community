<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function login(Request $request)// Recibe una request que contiene unos datos, los cuales permitiran o no seguir con 
    // la solicitud
    {
        // Se intenta o comprueba que es usuario registrado, en base a los datos enviados en la solicitud, 
        // sí devuelve true el usuario pasa el filtro y esta autenticado(dentro del registro de autenticados) 
        // y luego la ejecucion sigue pasa dentro del condicional if
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();// se asigna el usuario recien autenticado a la variable $user
            $token = $user->createToken('MyApp')->accessToken;// Se crea un Token de acceso a la api para el ususario
            return response()->json([
                'message' => 'User loged successfully.',
                'token' => $token
            ], 200);// retorna un json con un mensaje de éxito, el token de acceso y un estado 200.
        } else {// SI el intento devuelve false
            return response()->json(['message' => 'Unauthorized.'], 201);
            // Se devuelve una respuesta en json con mensaje Unauthorized y estado 201 
        }
    }
}
