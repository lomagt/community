<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\CommunityLink;
use Illuminate\Http\Request;
use App\Http\Requests\CommunityLinkForm;
use App\Queries\CommunityLinksQuery;
use Illuminate\Support\Facades\Auth;

class CommunityLinkController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->exists('text')) {
            $links = (new CommunityLinksQuery)->getSearchLinks(request()->exists('text'));
        } else if (request()->exists('popular')) {
            $links = (new CommunityLinksQuery)->getMostPopular();
        } else {
            $links = (new CommunityLinksQuery)->getAll();
        }


        return response()->json(['Links' => $links], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommunityLinkForm $request)
    {
        $link = new CommunityLink();
        $link->user_id = Auth::id();

        $approved = Auth::User()->isTrusted() ? true : false;
        $request->merge(['user_id' => Auth::id(), 'approved' => $approved]);

        if ($link->hasAlreadyBeenSubmitted($request->link)) {
        } else {
            CommunityLink::create($request->all());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityLink $communitylink)
    {
        $linkId = $communitylink;
        return response()->json(['Links' => $linkId], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommunityLink $communityLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityLink $communitylink)
    {
        $linkId = $communitylink->delete();
        return response()->json(['Links' => $linkId], 200);
    }
}
