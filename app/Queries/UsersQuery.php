<?php

namespace App\Queries;

use App\Models\User;
use App\Http\Requests\FileUserAvatarForm;

class UsersQuery
{

    public function getUsersUnTrusted()
    {
        $users = User::with(['hasLinks' => function ($query) {
            $query->where('approved', false);
        }])->where('trusted', false)->get();


        // foreach ($users as $us){
        //     if($us->hasLinks){
        //         foreach($us->hasLinks as $link){
        //             dump($link->title);
        //         }
        //     }
        // }

        return $users;
    }

    public function setFileAvatarInUser(User $user, FileUserAvatarForm $request)
    {
        if ($request->hasFile('imagen') && $request->file('imagen')->isValid()) {


            $user->image = asset('storage/' . $request->file('imagen')->hashName());
            $user->save();

            $request->file('imagen')->store('public');

            return true;
        } else {
            return false;
        }
    }
}
