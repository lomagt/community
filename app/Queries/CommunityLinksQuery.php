<?php

namespace App\Queries;

use App\Models\Channel;
use App\Models\CommunityLink;

class CommunityLinksQuery
{
    public function getByChannel(Channel $channel)
    {
        dd($channel);
        $canal = $channel->communitylinks()->where('approved', true)->latest('updated_at')->paginate(15);
        return $canal;
    }

    public function getAll()
    {
        $canal = CommunityLink::where('approved', true)->latest('updated_at')->paginate(15);
        return $canal;
    }

    public function getMostPopular()
    {
        $canal = CommunityLink::withCount('users')->where('approved', true)->orderBy('users_count', 'desc')->latest('updated_at')->paginate(15);
        return $canal;
    }

    public function getMostPopularWithChannel(Channel $channel)
    {
        $canal = $channel->communitylinks()->withCount('users')->where('approved', true)->orderBy('users_count', 'desc')->latest('updated_at')->paginate(15);
        return $canal;
    }

    public function getSearchLinks($searchFilters)
    {
        $searchFilters = preg_split('/\s+/', $searchFilters, -1, PREG_SPLIT_NO_EMPTY);

        $canal = CommunityLink::where(function ($query) use ($searchFilters) {
            foreach ($searchFilters as $value) {
                $query->orWhere([
                    ['title', 'like', "%{$value}%"],
                    ['approved', true]
                ]);
            }
        })
            ->latest('updated_at')
            ->paginate(15);


        return $canal;
    }

    public function getSearchLinksWithPopular($searchFilters)
    {
        $searchFilters = explode('&', $searchFilters);

        $searchFilters = preg_split('/\s+/', $searchFilters[0], -1, PREG_SPLIT_NO_EMPTY);

        $canal = CommunityLink::withCount('users')->where(function ($query) use ($searchFilters) {
            foreach ($searchFilters as $value) {
                $query->orWhere([
                    ['title', 'like', "%{$value}%"],
                    ['approved', true]
                ]);
            }
        })
            ->orderBy('users_count', 'desc')
            ->latest('updated_at')
            ->paginate(15);


        return $canal;
    }

    // public function getLinksUnapproved()
    // {        
    //     $linksUnapproved = CommunityLink::with(['creator' => function($query){
    //         $query->where('trusted', false);
    //     }])->where('approved', false)->get();

    //     return $linksUnapproved;
    // }
}
