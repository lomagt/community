<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Queries\UsersQuery;

class sendlink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendmail:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $unpprovedLinks = (new UsersQuery)->getUsersUnTrusted();
        
        if ($unpprovedLinks) {
            $details = [

                'title' => 'Mail from CommunityLinksSolutionsLaravel.com',

                'body' => 'recently uploaded link needs admin approval'

            ];



            \Mail::to('manasalfaro@gmail.com')->send(new \App\Mail\MyTestMail($details));
        }
    }
}
